---
name: Home
route: /
---

# Dashboard to The Full Web

... including your private data, linking between your network and the oh-so-public internet.

** "Start simply & encourage evolution." **


## Philosophy & Design

Everything starts small and local. (... and we believe that everything truly meaningful stays there!) This project helps you start: use it to maintain your private and public data, sync with others and build your networks, and grow beyond them where necessary.

A reference implementation of this approach is [Distrinet](https://github.com/trentlarson/distrinet/), a personal data index to organize your data and link to the rest of the web. It supports the example apps.

This is part of a worldview that prioritizes people and their self-determination and better interactions (with other humans, not with machines). What else supports this?

- Tools: Bitcoin and all kinds of cryptoassets.

- Other: [MyData](https://mydata.org/). [The work of Ink & Switch](https://www.inkandswitch.com/local-first/). The [2019 Decentralized Web Summit](https://decentralizedweb.net).


#### Goals & Guidelines

- Support simple organization, for beginners...
  - The simplest and most ubiquitous mode of sharing is direct file sharing, eg Dropbox & Google Drive & OneDrive. Someday this tool may have an embedded file sharing app (eg Syncthing) which could be the easiest way for someone to begin.
  - File-based addressing is pretty straightforward, so we should be able to make it easy to map between identifiers (eg. URIs) and locations.
- ... and easily evolve to advanced uses.
  - As a user gets more adept they will have more preference over how they share, so this should always support the individual's preferred method of backup and permissioned access. This includes rsync, git over SSH, CRDTs, etc. Tools dedicated to that function will always beat this tool because this has a different focus: to manage and investigate the shared data.
  - I expect it will be straightforward for a project to migrate into some richer structure, eg. a DB or Mastodon or a centralized, permissioned service. I expect those tools would have APIs to assist this migration.

Local Data

- Each repo will have a URI.
- Individuals can manage the URLs for that URI.
  - It may be that it's public data, with one URL for everyone to share.
  - But for private data:
    - Each client of that repo may have a different URL that URI can be found, eg. a path on the local filesystem.
    - Each URI might be hosted on a sometimes-available networked source.  So it's useful to point to a cached location as well for offline access (which is potentially stale).
    - There may be credential information required to access a networked source.
    - Note that this is much like a DID Document, and we may want to fully go that route.
- Must run locally (w/o internet) w/ filesystem access (to support the simplest form of sharing).
- Want mobile because that's a significant platform for daily use, and task completion isn't always done near your larger device.
- I can migrate to a localhost app using the same settings.
  - Different UI
  - Different DB

- Identity
  - Support the signing, verification, and storage of that historical info.
  - This may be embedded in the tasks, though I'm sure we'll want to be able to extract data without that baggage as well.

- Transmission & Storage
  - Personal devices are the start of recording. Expansion to third-party hosts should be an easy step... as should extraction from the host.


## Features

- Track all your private folders, both local and remote. (... and public folders, too, if you like.)
- Automatically keep a local copy of remote data.
- Index all your data, for your own use and in preparation for broader distribution.
- Generate your own Decentralized IDs for signing your contributions.

Future Features

- Back up those folders (eg. by copying to an external drive, and someday by syncing changes only).
- Keep a record of your friends' Decentralized IDs, and validate their work.
- See [Distrinet itself](https://github.com/trentlarson/distrinet) for more up-to-date details.

Future Epics (AKA Big Features)

- Give an easy-to-use interface to sharing & vetting content via file syncing, git, Matrix, Keybase.io, maybe even Yjs & CRDTs, etc. (This is an ever-expanding set of features to grow the use of diffs, verifiable data, and permissions management.)
  - Much of this will work it's way into wallets or agents or git as the project grows and/or the tools mature.
  - Types of edits:
    - file
      - directly (potentially followed by a publication)
      - into my copy with a diff
    - git
      - directly into repo
      - into a repo with a merge request (or with a `git diff` patch someday)
    - CRDTs: directly
    - DB
      - shared via file sync (eg. SQLite)
      - extract a patch (either via diff or changeset-since-version)

- Show evolutionary paths toward more interoperability.
  - Integrate with a mobile identity wallet, eg. [endorser-mobile](https://github.com/trentlarson/endorser-mobile/blob/master/README.md)
  - Integrate with discovery, even publicly, eg. [endorser-ch](https://github.com/trentlarson/endorser-ch/blob/master/README.md)


## Example Apps

- [Distributed Task Lists](https://github.com/trentlarson/distrinet/blob/master/app/features/task-lists/README.md)

- [Distributed Histories](https://github.com/trentlarson/distrinet/blob/master/app/features/histories/README.md)

- [Distributed Genealogy](https://github.com/trentlarson/distrinet/blob/master/app/features/genealogy/README.md)

  - You can see [a genealogy-based presentation and app demo here](https://www.familysearch.org/rootstech/rtc2021/session/the-global-genealogical-forest-with-a-distributed-network-of-roots).
